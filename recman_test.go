package main

import (
	"context"
	"net"
	"os"
	"path"
	"path/filepath"
	"testing"

	pb "bbbatscale.de/recman/recpb"
	bolt "go.etcd.io/bbolt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	healthpb "google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/fieldmaskpb"
)

func TestRecordings(t *testing.T) {
	dir, err := os.MkdirTemp("", t.Name())
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	dbFile := filepath.Join(dir, "recordings.db")
	db, err := bolt.Open(dbFile, 0644, nil)
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()
	db.NoSync = true // we don't care about test data's durability

	recMan := newRecMan(&boltKV{db: db, bucket: []byte("recordings")})
	health := &healthChecker{recordings: recMan}
	srv := grpc.NewServer()
	pb.RegisterRecordingsServer(srv, recMan)
	healthpb.RegisterHealthServer(srv, health)

	// let the OS allocate an unused port for us
	lis, err := net.Listen("tcp", "[::1]:0")
	if err != nil {
		t.Fatal(err)
	}
	defer lis.Close()
	go srv.Serve(lis)

	addr := lis.Addr().String()
	cc, err := grpc.Dial(addr, grpc.WithBlock(), grpc.WithInsecure())
	if err != nil {
		t.Fatal(err)
	}
	defer cc.Close()

	ctx := context.Background()
	cli := pb.NewRecordingsClient(cc)

	// Setup done, run sub-tests

	t.Run("CreateInvalidArgument", func(t *testing.T) {
		req := &pb.CreateRecordingRequest{}
		_, err := cli.CreateRecording(ctx, req)
		if err == nil || status.Code(err) != codes.InvalidArgument {
			t.Errorf("got error %v, want InvalidArgument", err)
		}

		req.Recording = &pb.Recording{Name: fullName(t.Name())}
		_, err = cli.CreateRecording(ctx, req)
		if err == nil || status.Code(err) != codes.InvalidArgument {
			t.Errorf("got error %v, want InvalidArgument", err)
		}

		req.Recording.Origin = "self"
		_, err = cli.CreateRecording(ctx, req)
		if err != nil {
			t.Errorf("got error %v, want nil", err)
		}
	})
	t.Run("CreateAlreadyExists", func(t *testing.T) {
		req := &pb.CreateRecordingRequest{
			Recording: &pb.Recording{
				Name:   fullName(t.Name()),
				Origin: t.Name(),
			},
		}
		_, err := cli.CreateRecording(ctx, req)
		if err != nil {
			t.Errorf("got error %v, want nil", err)
		}
		_, err = cli.CreateRecording(ctx, req)
		if err == nil || status.Code(err) != codes.AlreadyExists {
			t.Errorf("got error %v, want AlreadyExists", err)
		}
	})
	t.Run("GetNotFound", func(t *testing.T) {
		req := &pb.GetRecordingRequest{
			Name: fullName(t.Name()),
		}
		_, err := cli.GetRecording(ctx, req)
		if err == nil || status.Code(err) != codes.NotFound {
			t.Errorf("got error %v, want NotFound", err)
		}

		createReq := &pb.CreateRecordingRequest{
			Recording: &pb.Recording{
				Name:   fullName(t.Name()),
				Origin: t.Name(),
			},
		}
		if _, err := cli.CreateRecording(ctx, createReq); err != nil {
			t.Fatal(err)
		}

		resp, err := cli.GetRecording(ctx, req)
		if err != nil {
			t.Fatalf("got error %v, want nil", err)
		}
		if resp.Name != req.Name {
			t.Errorf("got name %s in response, want %s", resp.Name, req.Name)
		}
	})
	t.Run("Update", func(t *testing.T) {
		recording := &pb.Recording{
			Name:   fullName(t.Name()),
			Origin: t.Name(),
			State:  pb.Recording_ARCHIVING,
		}
		getReq := &pb.GetRecordingRequest{
			Name: recording.Name,
		}
		createReq := &pb.CreateRecordingRequest{
			Recording: recording,
		}
		if _, err := cli.CreateRecording(ctx, createReq); err != nil {
			t.Fatal(err)
		}
		mask, err := fieldmaskpb.New(recording, "state")
		if err != nil {
			t.Fatal(err)
		}
		recording.State = pb.Recording_PUBLISHING
		req := &pb.UpdateRecordingRequest{
			Recording:  recording,
			UpdateMask: mask,
		}

		if _, err := cli.UpdateRecording(ctx, req); err != nil {
			t.Fatal(err)
		}

		got, err := cli.GetRecording(ctx, getReq)
		if err != nil {
			t.Fatal(err)
		}

		if got.State != pb.Recording_PUBLISHING {
			t.Errorf("got state %s, want PUBLISHING", got.State)
		}

		// It shouldn't be possible to mutate a recording's name or
		// origin after creation. Let's try whether it is.
		mask, err = fieldmaskpb.New(recording, "name", "origin", "state")
		if err != nil {
			t.Fatal(err)
		}
		req.UpdateMask = mask
		req.Recording.Origin = "XXX"

		_, err = cli.UpdateRecording(ctx, req)
		if err == nil || status.Code(err) != codes.InvalidArgument {
			t.Errorf("got error %v, want InvalidArgument", err)
		}
	})
	t.Run("UpdateStateMustNotMoveBackwards", func(t *testing.T) {
		recording := &pb.Recording{
			Name:   fullName(t.Name()),
			Origin: t.Name(),
			State:  pb.Recording_READY,
		}
		getReq := &pb.GetRecordingRequest{
			Name: recording.Name,
		}
		createReq := &pb.CreateRecordingRequest{
			Recording: recording,
		}
		if _, err := cli.CreateRecording(ctx, createReq); err != nil {
			t.Fatal(err)
		}
		mask, err := fieldmaskpb.New(recording, "state")
		if err != nil {
			t.Fatal(err)
		}
		recording.State = pb.Recording_PUBLISHED
		req := &pb.UpdateRecordingRequest{
			Recording:  recording,
			UpdateMask: mask,
		}

		if _, err := cli.UpdateRecording(ctx, req); err != nil {
			t.Fatal(err)
		}

		got, err := cli.GetRecording(ctx, getReq)
		if err != nil {
			t.Fatal(err)
		}

		// The field update above (from READY to PUBLISHED) should've
		// been ignored.
		if got.State != pb.Recording_READY {
			t.Errorf("got state %s, want READY", got.State)
		}
	})
	t.Run("DeleteAll", func(t *testing.T) {
		resp, err := cli.ListRecordings(ctx, &pb.ListRecordingsRequest{})
		if err != nil {
			t.Fatal(err)
		}
		for _, r := range resp.Recordings {
			delReq := &pb.DeleteRecordingRequest{
				Name: r.Name,
			}
			_, err := cli.DeleteRecording(ctx, delReq)
			if err != nil {
				t.Errorf("delete %s: %v", r.Name, err)
			}
		}
		resp, err = cli.ListRecordings(ctx, &pb.ListRecordingsRequest{})
		if err != nil {
			t.Fatal(err)
		}

		if n := len(resp.Recordings); n > 0 {
			var names []string
			for _, r := range resp.Recordings {
				names = append(names, r.Name)
			}
			t.Errorf("got %d recordings, want 0 (names: %v)", n, names)
		}
	})

}

func fullName(recID string) string {
	return path.Join("recordings", recID)
}
