package main

import (
	"context"
	"encoding/hex"
	"net"
	"os"
	"path/filepath"
	"testing"

	pb "bbbatscale.de/recman/recpb"
	bolt "go.etcd.io/bbolt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/fieldmaskpb"
)

func TestUploadService(t *testing.T) {
	if os.Getenv("CEPH_USER") == "" {
		t.Skip("skipping test, set CEPH_USER to run")
	}
	ctx := context.Background()

	_, usrv, cc, teardown, err := setupUploadsTestServer()
	if err != nil {
		t.Fatal(err)
	}
	defer teardown()

	ceph, err := mountCephFS(ctx, "/u/uit/recordings.test")
	if err != nil {
		t.Fatal(err)
	}
	defer ceph.unmount(ctx)

	usrv.store = ceph

	rec := pb.NewRecordingsClient(cc)
	req := &pb.CreateRecordingRequest{
		Recording: &pb.Recording{
			Name:   fullName(t.Name()),
			Origin: t.Name(),
		},
	}
	if _, err := rec.CreateRecording(ctx, req); err != nil {
		t.Errorf("got error %v, want nil", err)
	}

	cli := pb.NewUploadsClient(cc)
	parent := fullName(t.Name())
	t.Run("Create", func(t *testing.T) {
		req := &pb.CreateUploadRequest{
			Parent: parent,
		}
		upload, err := cli.CreateUpload(ctx, req)
		if err != nil {
			t.Fatal(err)
		}

		getReq := &pb.GetUploadRequest{
			Name: upload.Name,
		}
		got, err := cli.GetUpload(ctx, getReq)
		if err != nil {
			t.Fatal(err)
		}
		if got.Name != upload.Name {
			t.Errorf("got upload %s, want %s", got.Name, upload.Name)
		}
	})
	t.Run("GetNotFound", func(t *testing.T) {
		getReq := &pb.GetUploadRequest{
			Name: uploadName(parent, "does-not-exist"),
		}
		_, err := cli.GetUpload(ctx, getReq)
		if err == nil || status.Code(err) != codes.NotFound {
			t.Errorf("got error %v, want NotFound", err)
		}
	})
	t.Run("Update", func(t *testing.T) {
		req := &pb.CreateUploadRequest{
			Parent: parent,
		}
		upload, err := cli.CreateUpload(ctx, req)
		if err != nil {
			t.Fatal(err)
		}

		mask, err := fieldmaskpb.New(upload, "length")
		if err != nil {
			t.Fatal(err)
		}

		updateReq := &pb.UpdateUploadRequest{
			Upload: &pb.Upload{
				Name:   upload.Name,
				Length: 8 << 30, // greater than INT32_MAX
				Done:   true,    // NOTE: not included in mask, must not be written
			},
			UpdateMask: mask,
		}

		got, err := cli.UpdateUpload(ctx, updateReq)
		if err != nil {
			t.Fatal(err)
		}

		if got.Length != 8<<30 {
			t.Errorf("got length %d, want %d", got.Length, 8<<30)
		}
		if got.Done {
			t.Errorf("got done=%t, want done=0", got.Done)
		}
	})
	t.Run("DeleteAll", func(t *testing.T) {
		listReq := &pb.ListUploadsRequest{
			Parent: parent,
		}
		resp, err := cli.ListUploads(ctx, listReq)
		if err != nil {
			t.Fatal(err)
		}

		for _, u := range resp.Uploads {
			delReq := &pb.DeleteUploadRequest{
				Name: u.Name,
			}
			if _, err := cli.DeleteUpload(ctx, delReq); err != nil {
				t.Errorf("delete %s: %v", u.Name, err)
			}
		}

		resp, err = cli.ListUploads(ctx, listReq)
		if err != nil {
			t.Fatal(err)
		}

		if n := len(resp.Uploads); n > 0 {
			var names []string
			for _, u := range resp.Uploads {
				names = append(names, u.Name)
			}
			t.Errorf("got %d uploads, want 0 (names: %v)", n, names)
		}

	})
}

func TestUploadStreamingWrite(t *testing.T) {
	if os.Getenv("CEPH_USER") == "" {
		t.Skip("skipping test, set CEPH_USER to run")
	}
	ctx := context.Background()

	ceph, err := mountCephFS(ctx, "/u/uit/recordings.test")
	if err != nil {
		t.Fatal(err)
	}
	defer ceph.unmount(ctx)

	_, usrv, cc, teardown, err := setupUploadsTestServer()
	if err != nil {
		t.Fatal(err)
	}
	defer teardown()
	usrv.store = ceph

	cli := pb.NewUploadsClient(cc)
	parent := fullName(t.Name())

	rec := pb.NewRecordingsClient(cc)
	createRecordingReq := &pb.CreateRecordingRequest{
		Recording: &pb.Recording{
			Name:   parent,
			Origin: t.Name(),
		},
	}
	if _, err := rec.CreateRecording(ctx, createRecordingReq); err != nil {
		t.Errorf("got error %v, want nil", err)
	}

	createReq := &pb.CreateUploadRequest{
		Parent: parent,
	}
	upload, err := cli.CreateUpload(ctx, createReq)
	if err != nil {
		t.Fatal(err)
	}

	content := make([]byte, 256)
	for i := range content {
		content[i] = byte(i)
	}
	req := &pb.WriteRequest{
		Name:    upload.Name,
		Content: content,
		Offset:  0,
	}

	total := int64(10<<20 + 13654)

	stream, err := cli.StreamingWrite(ctx)
	if err != nil {
		t.Fatal(err)
	}

	off := int64(0)
	for off < total {
		left := total - off
		if left < int64(len(req.Content)) {
			req.Content = req.Content[:int(left)]
		}
		if err := stream.Send(req); err != nil {
			t.Fatal(err)
		}
		off += int64(len(req.Content))
		req.Offset = off
	}


	resp, err := stream.CloseAndRecv()
	if err != nil {
		t.Fatal(err)
	}
	if resp.NewOffset != total {
		t.Errorf("got newoff %d, want %d", resp.NewOffset, total)
	}
}

func TestUploadWrite(t *testing.T) {
	if os.Getenv("CEPH_USER") == "" {
		t.Skip("skipping test, set CEPH_USER to run")
	}
	ctx := context.Background()

	ceph, err := mountCephFS(ctx, "/u/uit/recordings.test")
	if err != nil {
		t.Fatal(err)
	}
	defer ceph.unmount(ctx)

	_, usrv, cc, teardown, err := setupUploadsTestServer()
	if err != nil {
		t.Fatal(err)
	}
	defer teardown()
	usrv.store = ceph

	cli := pb.NewUploadsClient(cc)
	parent := fullName(t.Name())

	rec := pb.NewRecordingsClient(cc)
	createRecordingReq := &pb.CreateRecordingRequest{
		Recording: &pb.Recording{
			Name:   parent,
			Origin: t.Name(),
		},
	}
	if _, err := rec.CreateRecording(ctx, createRecordingReq); err != nil {
		t.Errorf("got error %v, want nil", err)
	}

	createReq := &pb.CreateUploadRequest{
		Parent: parent,
	}
	upload, err := cli.CreateUpload(ctx, createReq)
	if err != nil {
		t.Fatal(err)
	}

	// upload zero bytes
	content := make([]byte, 1<<20)
	totalLen := int64(10<<20 + 12345)

	req := &pb.WriteRequest{
		Name:    upload.Name,
		Content: content,
		Offset:  42, // for fun, let's start with a bogus offset
	}

	_, err = cli.Write(ctx, req)
	if err == nil || status.Code(err) != codes.Aborted {
		t.Fatalf("got error %v, want Aborted", err)
	}

	off := int64(0)
	for off < totalLen {
		req.Offset = off
		if n := totalLen - off; n < int64(len(req.Content)) {
			req.Content = req.Content[:n]
		}
		resp, err := cli.Write(ctx, req)
		if err != nil {
			t.Fatal(err)
		}
		expectOff := off + int64(len(req.Content))
		if resp.NewOffset != expectOff {
			t.Fatalf("got offset %d, want %d", resp.NewOffset, expectOff)
		}
		off = resp.NewOffset
	}

	// sha256('\0'^10498105)
	expectSHA2, _ := hex.DecodeString(
		"571606e5460d2a24d5ee8410d85b9ea770d5c224edde87dccf733d8d779590f6")

	mask, err := fieldmaskpb.New(upload, "length", "done", "sha256")
	if err != nil {
		t.Fatal(err)
	}

	updateReq := &pb.UpdateUploadRequest{
		Upload: &pb.Upload{
			Name:   upload.Name,
			Length: totalLen,
			Done:   true,
			Sha256: expectSHA2,
		},
		UpdateMask: mask,
	}

	if _, err = cli.UpdateUpload(ctx, updateReq); err != nil {
		t.Fatal(err)
	}
}

func setupUploadsTestServer() (rec *recMan, usrv *uploadService, client *grpc.ClientConn, teardown func(), err error) {
	dir, err := os.MkdirTemp("", "recman-test.")
	if err != nil {
		return nil, nil, nil, nil, err
	}
	defer func() {
		if err != nil {
			_ = os.RemoveAll(dir)
		}
	}()

	dbFile := filepath.Join(dir, "uploads.db")
	db, err := bolt.Open(dbFile, 0644, nil)
	if err != nil {
		return nil, nil, nil, nil, err
	}
	db.NoSync = true // we don't care about test data's durability
	defer func() {
		if err != nil {
			_ = db.Close()
		}
	}()

	// XXX
	if (&boltKV{db: db, bucket: uploadsBucket}).ensureBucketExists(); err != nil {
		panic(err)
	}
	if (&boltKV{db: db, bucket: recordingsBucket}).ensureBucketExists(); err != nil {
		panic(err)
	}

	usrv = &uploadService{
		db: db,
	}
	recMan := newRecMan(&boltKV{db: db, bucket: recordingsBucket})
	srv := grpc.NewServer()
	pb.RegisterUploadsServer(srv, usrv)
	pb.RegisterRecordingsServer(srv, recMan)

	// let the OS allocate an unused port for us
	lis, err := net.Listen("tcp", "[::1]:0")
	if err != nil {
		return nil, nil, nil, nil, err
	}
	defer func() {
		if err != nil {
			_ = lis.Close()
		}
	}()
	go srv.Serve(lis)

	addr := lis.Addr().String()
	cc, err := grpc.Dial(addr, grpc.WithBlock(), grpc.WithInsecure())
	if err != nil {
		return nil, nil, nil, nil, err
	}

	teardown = func() {
		_ = cc.Close()
		_ = lis.Close()
		_ = db.Close()
		_ = os.RemoveAll(dir)
	}

	return recMan, usrv, cc, teardown, nil
}
