package main

/*
#cgo LDFLAGS: -lcephfs
#cgo CPPFLAGS: -D_FILE_OFFSET_BITS=64
#include <stdlib.h>
#include <cephfs/libcephfs.h>
*/
import "C"
import (
	"syscall"
	"unsafe"

	"github.com/ceph/go-ceph/cephfs"
)

func cephfsMkdirs(mnt *cephfs.MountInfo, path string, mode uint32) error {
	cPath := C.CString(path)
	defer C.free(unsafe.Pointer(cPath))

	ptr := *(*unsafe.Pointer)(unsafe.Pointer(mnt))
	cMount := (*C.struct_ceph_mount_info)(ptr)

	rc := C.ceph_mkdirs(cMount, cPath, C.mode_t(mode))
	if rc == 0 {
		return nil
	}
	return syscall.Errno(-rc)
}
