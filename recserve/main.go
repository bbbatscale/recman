package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	"bbbatscale.de/recman/recpb"
	"github.com/julienschmidt/httprouter"
	"go.uber.org/zap"
	"go.uber.org/zap/zapgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/grpclog"
)

var (
	laddr = flag.String("listen", os.Getenv("RECSERVE_LISTEN"),
		"local address to serve HTTP")
	certFile = flag.String("cert", os.Getenv("RECSERVE_CERT"),
		"PEM-encoded X.509 certificate for TLS")
	keyFile = flag.String("key", os.Getenv("RECSERVE_KEY"),
		"private key corresponding to cert")
	noTLS = flag.Bool("notls", os.Getenv("RECSERVE_NOTLS") != "",
		"serve plain-text HTTP instead of HTTPS")

	recServerAddr = flag.String("rec-addr", os.Getenv("RECSERVE_RECMAN_ADDR"),
		"connect to recordings API at `host:port`")
	recCert = flag.String("rec-cert",
		getEnvDefault("RECSERVE_CLIENT_CERT", "/etc/recserve/cert.pem"),
		"`path` to PEM-encoded client certificate for recordings API")
	recKey = flag.String("rec-key",
		getEnvDefault("RECSERVE_CLIENT_KEY", "/etc/recserve/key.pem"),
		"`path` to private key matching certificate for recordings API")
	recTrust = flag.String("rec-trust", os.Getenv("RECSERVE_RECMAN_TRUST"),
		"`path` to custom trust anchors used to authenticate recordings server")
	cephSub = flag.String("cephsub", os.Getenv("RECSERVE_CEPH_SUB"),
		"Ceph file system path to use as root")

	httpWriteTimeout = flag.Duration("wrtimeout",
		tryParseDuration(getEnvDefault("RECSERVE_WRTIMEOUT", "120s")),
		"timeout response writing after `duration`")
)

func main() {
	flag.Parse()
	logger, err := zap.NewProduction()
	if err != nil {
		// Just tear it down already.
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	zap.RedirectStdLog(logger)
	grpclog.SetLoggerV2(zapgrpc.NewLogger(logger))
	log := logger.Sugar()

	ceph, err := mountCephFS(context.Background(), *cephSub)
	if err != nil {
		log.Fatal(err)
	}
	defer ceph.unmount(context.Background())

	clientTLSConf, err := makeTLSConfig(*recCert, *recKey, *recTrust)
	if err != nil {
		log.Fatal(err)
	}
	conn, err := grpc.Dial(*recServerAddr,
		grpc.WithTransportCredentials(credentials.NewTLS(clientTLSConf)),
		grpc.WithBlock(),
	)
	if err != nil {
		log.Fatalf("dial %s: %v", *recServerAddr, err)
	}
	defer conn.Close()

	rpcClient := recpb.NewRecordingsClient(conn)
	recSrv := newRecordingsServer(rpcClient, ceph, withLogger(logger))

	var serverTLSConf *tls.Config
	if !*noTLS {
		conf, err := makeTLSConfig(*certFile, *keyFile, "")
		if err != nil {
			log.Fatal(err)
		}
		conf.NextProtos = []string{"h2", "http/1.1"}
		serverTLSConf = conf
	}

	// Only sufficient as long as we use WithBlock when dialing.
	readyz := func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}

	mux := httprouter.New()
	mux.Handler("GET", "/r/:recording/*path", recSrv)
	mux.Handler("HEAD", "/r/:recording/*path", recSrv)
	mux.Handler("GET", "/r/:recording", recSrv)
	mux.Handler("HEAD", "/r/:recording", recSrv)
	mux.HandlerFunc("GET", "/readyz", readyz)
	mux.HandlerFunc("HEAD", "/readyz", readyz)
	mux.HandlerFunc("GET", "/livez", recSrv.livez)
	mux.HandlerFunc("HEAD", "/livez", recSrv.livez)

	srv := &http.Server{
		Addr:         *laddr,
		Handler:      mux,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: *httpWriteTimeout,
		TLSConfig:    serverTLSConf,
	}

	log.Infof("listen on %s", *laddr)
	if *noTLS {
		log.Fatal(srv.ListenAndServe())
	} else {
		log.Fatal(srv.ListenAndServeTLS("", ""))
	}
}

func makeTLSConfig(certFile, keyFile, caFile string) (tlsConf *tls.Config, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("makeTLSConfig(%q, %q, %q): %w",
				certFile, keyFile, caFile, err)
		}
	}()
	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return nil, err
	}
	var caCerts *x509.CertPool
	if caFile != "" {
		// custom trust anchors requested
		pemBytes, err := os.ReadFile(caFile)
		if err != nil {
			return nil, err
		}
		caCerts = x509.NewCertPool()
		if !caCerts.AppendCertsFromPEM(pemBytes) {
			return nil, fmt.Errorf("no trust anchors read from %q", caFile)
		}
	}

	tlsConf = &tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      caCerts,
		MinVersion:   tls.VersionTLS12,
	}

	return tlsConf, nil
}

func getEnvDefault(key, def string) string {
	if v := os.Getenv(key); v != "" {
		return v
	}
	return def
}

func tryParseDuration(s string) time.Duration {
	if d, err := time.ParseDuration(s); err == nil {
		return d
	}
	return 0
}
