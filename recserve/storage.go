package main

import (
	"context"
	"fmt"
	"io"
	"os"
	"path"
	"time"

	"github.com/ceph/go-ceph/cephfs"
	"github.com/ceph/go-ceph/rados"
	"golang.org/x/sys/unix"
)

type cephFSStore struct {
	mount *cephfs.MountInfo
	sub   string
}

func mountCephFS(ctx context.Context, sub string) (store *cephFSStore, err error) {
	c, err := rados.NewConnWithUser(os.Getenv("CEPH_USER"))
	if err != nil {
		return nil, err
	}
	mnt, err := cephfs.CreateFromRados(c)
	if err != nil {
		return nil, err
	}

	if err := mnt.ReadDefaultConfigFile(); err != nil {
		return nil, fmt.Errorf("ceph_conf_read_file: %v", err)
	}
	if err := mnt.ParseDefaultConfigEnv(); err != nil {
		return nil, fmt.Errorf("ceph_conf_parse_env: %v", err)
	}
	if err := mnt.Init(); err != nil {
		return nil, fmt.Errorf("ceph_init: %v", err)
	}
	if err := mnt.SetMountPerms(cephfs.NewUserPerm(0, 0, nil)); err != nil {
		return nil, fmt.Errorf("ceph_mount_perms_set: %v", err)
	}

	if err := mnt.MountWithRoot(sub); err != nil {
		return nil, fmt.Errorf("ceph_mount: %v", err)
	}

	s := &cephFSStore{
		mount: mnt,
		sub:   sub,
	}

	return s, err
}

func (s *cephFSStore) ping(ctx context.Context) error {
	if _, err := s.mount.StatFS("/"); err != nil {
		return err
	}
	return nil
}

func (s *cephFSStore) unmount(ctx context.Context) error {
	if err := s.mount.Unmount(); err != nil {
		return err
	}
	return s.mount.Release()
}

type zipFile interface {
	io.ReaderAt
	io.Closer
	Size() int64
	ModTime() time.Time
}

// CephfsFile implements a zipFile backed by CephFS. The size is retrieved once
// on open to avoid a stat with no way to signal errors. Recordings are
// immutable so sizes won't change.
type cephfsFile struct {
	*cephfs.File
	size  int64
	mtime time.Time
}

func (f cephfsFile) Size() int64 {
	return f.size
}

func (f cephfsFile) ModTime() time.Time {
	return f.mtime
}

func (s *cephFSStore) openRecordingZip(ctx context.Context, recording string) (f zipFile, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("openRecordingZip: %w", err)
		}
	}()

	dataFile, err := recordingZipPath(recording)
	if err != nil {
		return nil, err
	}

	fd, err := s.mount.Open(dataFile, os.O_RDONLY, 0)
	if err != nil {
		return nil, err
	}
	st, err := fd.Fstatx(cephfs.StatxSize|cephfs.StatxMtime, 0)
	if err != nil {
		fd.Close()
		return nil, err
	}

	f = cephfsFile{
		File:  fd,
		size:  int64(st.Size),
		mtime: time.Unix(0, unix.TimespecToNsec(unix.Timespec(st.Mtime))),
	}

	return f, nil
}

func recordingZipPath(r string) (string, error) {
	cleaned := path.Clean(r) // process dot/dotdot and strip trailing slashes
	parent, child := path.Split(cleaned)
	if parent != "recordings/" || len(child) < 2 {
		return "", fmt.Errorf("invalid recording identifier: %q", r)
	}
	bucket := child[:2]
	return path.Join(parent, bucket, child, "data"), nil
}
