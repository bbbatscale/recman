package main

import (
	"archive/zip"
	"bytes"
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"path"
	"sort"
	"strconv"
	"strings"
	"time"

	pb "bbbatscale.de/recman/recpb"
	"bbbatscale.de/recman/recserve/player"
	"github.com/julienschmidt/httprouter"
	"go.uber.org/zap"
	"golang.org/x/crypto/blake2b"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type recordingsServer struct {
	rpc   pb.RecordingsClient
	store *cephFSStore
	log   *zap.Logger

	// immutable after initialization
	recPrefix string
	playerFS  fs.FS // read player HTML/JS files from here
}

type serverOption func(*recordingsServer)

func withLogger(l *zap.Logger) serverOption {
	return func(s *recordingsServer) {
		s.log = l
	}
}

func withImpliedPrefix(p string) serverOption {
	return func(s *recordingsServer) {
		s.recPrefix = p
	}
}

func withPlayerFS(fsys fs.FS) serverOption {
	return func(s *recordingsServer) {
		s.playerFS = fsys
	}
}

func newRecordingsServer(rpc pb.RecordingsClient, store *cephFSStore, opts ...serverOption) *recordingsServer {
	s := &recordingsServer{
		rpc:       rpc,
		store:     store,
		log:       zap.NewNop(),
		recPrefix: "recordings/",
		playerFS:  player.FS,
	}

	for _, opt := range opts {
		opt(s)
	}

	return s
}

func (s *recordingsServer) livez(w http.ResponseWriter, r *http.Request) {
	if err := s.store.ping(r.Context()); err != nil {
		s.log.Warn("unavailable!", zap.Error(err))
		w.WriteHeader(503)
		return
	}
	w.WriteHeader(200)
}

func (s *recordingsServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	log := s.log.With(
		zap.String("host", r.Host),
		zap.String("remote_addr", r.RemoteAddr),
		zap.String("request_method", r.Method),
		zap.String("request_path", r.URL.Path),
		zap.String("request_uri", r.RequestURI), // unprocessed path from request line
		zap.String("xff", r.Header.Get("X-Forwarded-For")),
	)

	params := httprouter.ParamsFromContext(ctx)
	recID := params.ByName("recording")
	if strings.ContainsRune(recID, '/') {
		// This should've been prevented by our caller already.
		log.Warn("recording ID contains '/'", zap.String("recid", recID))
		http.Error(w, "invalid recording identifier", 400)
		return
	}
	recName := path.Join(s.recPrefix, recID)
	recPath := params.ByName("path")
	log = log.With(
		zap.String("recording", recName),
		zap.String("recording_path", recPath),
	)

	log.Info("handle request")
	getCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	rec, err := s.rpc.GetRecording(getCtx, &pb.GetRecordingRequest{Name: recName})
	if err != nil {
		if status.Code(err) == codes.NotFound {
			log.Info("no such recording", zap.Error(err))
			http.Error(w, http.StatusText(404), 404)
			return
		}
		log.Error("GetRecording failed", zap.Error(err))
		http.Error(w, http.StatusText(503), 503)
		return
	}
	cancel()
	if q := rec.State; q != pb.Recording_READY {
		log.Error("recording state is not READY",
			zap.String("recording_state", q.String()))
		http.Error(w, http.StatusText(403), 403)
		return
	}

	if !hasParam(params, "path") || recPath == "/" {
		newPath := path.Join(r.URL.Path, "index.html")
		if q := r.URL.RawQuery; q != "" {
			newPath += "?" + q
		}
		log.Info("no path, redirecting", zap.String("location", newPath))
		w.Header().Set("Location", newPath)
		w.WriteHeader(302)
		return
	}
	if recPath == "/metadata.xml" && len(rec.Metadata) > 0 && rec.Metadata[0] != '{' {
		log.Info("overriding metadata.xml")
		w.Header().Set("Cache-Control", "no-cache")
		http.ServeContent(w, r, "metadata.xml",
			rec.UpdateTime.AsTime(), bytes.NewReader(rec.Metadata))
		return
	}

	// If the requested file can be found within the bbb-playback player
	// files, serve it from there, otherwise defer to the recording zip.
	if fi, err := fs.Stat(s.playerFS, ioFSPath(recPath)); !errors.Is(err, fs.ErrNotExist) {
		if err != nil {
			log.Error("stat playerFS",
				zap.Error(err),
				zap.String("iofs_path", ioFSPath(recPath)),
			)
			http.Error(w, http.StatusText(500), 500)
			return
		}
		content, err := fs.ReadFile(s.playerFS, ioFSPath(recPath))
		if err != nil {
			log.Error("read playerFS",
				zap.Error(err),
				zap.String("iofs_path", ioFSPath(recPath)),
			)
			http.Error(w, http.StatusText(500), 500)
			return
		}
		log.Info("serving player file",
			zap.String("file", fi.Name()),
			zap.Time("mtime", fi.ModTime()),
			zap.Int64("size", fi.Size()),
		)
		http.ServeContent(w, r, fi.Name(), fi.ModTime(), bytes.NewReader(content))
		return
	}

	if recPath == "/metadata.xml" {
		w.Header().Set("Cache-Control", "no-cache")
	} else {
		w.Header().Set("Cache-Control", "public, max-age=604800, immutable")
	}

	// If the recording has a content hash associated with it, we can use
	// it as a strong cache key.
	var etag string
	if alg := rec.ContentHash.GetAlg(); alg == pb.ContentHash_SHA256 {
		v := rec.ContentHash.GetValue()
		log.Info("have usable content hash",
			zap.String("sha256", fmt.Sprintf("%x", v)),
		)
		// Build the Etag by exploiting the recording's immutability:
		// any file within the recording is uniquely identified using
		// its container's content hash and the path within the
		// container.
		bsum := blake2b.Sum256(append(v, recPath...))
		etag = `"` + base64.URLEncoding.EncodeToString(bsum[:]) + `"`
		w.Header().Set("Etag", etag)
	}
	if v := r.Header.Get("If-None-Match"); v != "" && etag != "" {
		if strings.Contains(v, etag) {
			log.Info("not modified",
				zap.String("if-none-match", v),
				zap.String("etag", etag),
			)
			w.WriteHeader(http.StatusNotModified)
			return
		}
	}

	zfile, err := s.store.openRecordingZip(ctx, rec.Name)
	if err != nil {
		log.Error("openRecordingZip failed", zap.Error(err))
		http.Error(w, http.StatusText(503), 503)
		return
	}
	defer zfile.Close()
	log.Info("opened recording zip", zap.Int64("recording_size", zfile.Size()))

	if recPath == "/zip" {
		fname := recID + ".zip"
		w.Header().Set("Content-Length", strconv.FormatInt(zfile.Size(), 10))
		w.Header().Set("Content-Disposition", fmt.Sprintf(`attachment; filename=%q`, fname))
		sr := io.NewSectionReader(zfile, 0, zfile.Size())
		http.ServeContent(w, r, fname, zfile.ModTime(), sr)
		return
	}

	zr, err := zip.NewReader(zfile, zfile.Size())
	if err != nil {
		log.Error("zip.NewReader failed", zap.Error(err))
		http.Error(w, http.StatusText(503), 503)
		return
	}
	if r.Header.Get("Range") != "" {
		// For answering bytes range requests, the zip should hold them
		// uncompressed (method Store).
		zf, found := lookupZip(zr, ioFSPath(recPath))
		log.Info("lookupZip", zap.Bool("found", found))
		if found && zf.Method == zip.Store {
			raw, err := zf.OpenRaw()
			if err == nil {
				if rs, ok := raw.(io.ReadSeeker); ok {
					log.Info("serving raw zip data")
					http.ServeContent(w, r, recPath, time.Time{}, rs)
					return
				}
			}
		}
		log.Info("fallthrough ☹")
		// else: fallthrough to code below, which will 416
		// insatisfiable range requests
	}

	prefixToStrip := strings.TrimSuffix(r.URL.Path, recPath)
	next := http.StripPrefix(prefixToStrip, http.FileServer(http.FS(zr)))
	next.ServeHTTP(w, r)
}

// IoFSPath returns name adjusted for use with io/fs which uses unrooted paths.
func ioFSPath(name string) string {
	if name == "/" {
		return "."
	}
	return strings.TrimPrefix(name, "/")
}

func lookupZip(r *zip.Reader, name string) (*zip.File, bool) {
	i := sort.Search(len(r.File), func(i int) bool {
		return r.File[i].Name >= name
	})
	if i < len(r.File) && r.File[i].Name == name {
		return r.File[i], true
	}

	// fallback to linear search: unfortunately, we have a couple of zips
	// with unsorted directory.
	for _, f := range r.File {
		if f.Name == name {
			return f, true
		}
	}
	return nil, false
}

func hasParam(ps httprouter.Params, key string) bool {
	for i := range ps {
		if ps[i].Key == key {
			return true
		}
	}
	return false
}
