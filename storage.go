package main

import (
	"context"
	"crypto/sha256"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path"
	"strings"

	"github.com/ceph/go-ceph/cephfs"
	"github.com/ceph/go-ceph/rados"
)

type cephFSStore struct {
	mount *cephfs.MountInfo
	sub   string
}

func mountCephFS(ctx context.Context, sub string) (store *cephFSStore, err error) {
	c, err := rados.NewConnWithUser(os.Getenv("CEPH_USER"))
	if err != nil {
		return nil, err
	}
	mnt, err := cephfs.CreateFromRados(c)
	if err != nil {
		return nil, err
	}

	if err := mnt.ReadDefaultConfigFile(); err != nil {
		return nil, fmt.Errorf("ceph_conf_read_file: %v", err)
	}
	if err := mnt.ParseDefaultConfigEnv(); err != nil {
		return nil, fmt.Errorf("ceph_conf_parse_env: %v", err)
	}
	if err := mnt.Init(); err != nil {
		return nil, fmt.Errorf("ceph_init: %v", err)
	}
	if err := mnt.SetMountPerms(cephfs.NewUserPerm(0, 0, nil)); err != nil {
		return nil, fmt.Errorf("ceph_mount_perms_set: %v", err)
	}

	if err := mnt.MountWithRoot(sub); err != nil {
		return nil, fmt.Errorf("ceph_mount: %v", err)
	}

	s := &cephFSStore{
		mount: mnt,
		sub:   sub,
	}

	return s, err
}

func (s *cephFSStore) unmount(ctx context.Context) error {
	if err := s.mount.Unmount(); err != nil {
		return err
	}
	return s.mount.Release()
}

func (s *cephFSStore) mkdirp(ctx context.Context, path string, perm fs.FileMode) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("(*cephFSStore).mkdirp: %w (path=%q mode=%s)", err, path, perm)
		}
	}()
	return cephfsMkdirs(s.mount, path, uint32(perm.Perm()))
}

func (s *cephFSStore) prepareTree(ctx context.Context, recording string) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("(*cephFSStore).prepareTree: %w (recording=%q)", err, recording)
		}
	}()
	rpath, err := recPath(recording)
	if err != nil {
		return err
	}

	uploadsDir := path.Join(rpath, "uploads")
	return s.mkdirp(ctx, uploadsDir, 0755)
}

func (s *cephFSStore) finalizeTree(ctx context.Context, recording, designatedUpload string) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("(*cephFSStore).finalizeTree: %w (recording=%q upload=%q)",
				err, recording, designatedUpload)
		}
	}()
	if !strings.HasPrefix(path.Clean(designatedUpload), path.Clean(recording)) {
		return fmt.Errorf("upload %q not associated with recording %q",
			designatedUpload, recording)
	}

	rpath, err := recPath(recording)
	if err != nil {
		return err
	}

	_, uploadID := path.Split(designatedUpload)
	uploadData := path.Join(rpath, "uploads", uploadID)
	recData := path.Join(rpath, "data")
	if err := s.mount.Rename(uploadData, recData); err != nil {
		return fmt.Errorf("rename(%q, %q): %w", uploadData, recData, err)
	}

	return s.purgeUploads(ctx, rpath)
}

func (s *cephFSStore) purgeUploads(ctx context.Context, recDir string) (err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("(*cephFSStore).purgeUploads: %w (recdir=%q)",
				err, recDir)
		}
	}()
	uploadsDir := path.Join(recDir, "uploads")
	dirfd, err := s.mount.OpenDir(uploadsDir)
	if err != nil {
		return fmt.Errorf("opendir(%q): %w", uploadsDir, err)
	}
	defer dirfd.Close()

	for {
		ent, rerr := dirfd.ReadDir()
		if rerr != nil {
			return fmt.Errorf("readdir: %w", rerr)
		}
		if ent == nil {
			// EOF
			break
		}
		if ent.Name() == "." || ent.Name() == ".." {
			continue
		}
		file := path.Join(uploadsDir, ent.Name())
		if unlinkErr := s.mount.Unlink(file); unlinkErr != nil && err == nil {
			err = fmt.Errorf("unlink(%q): %w", file, unlinkErr)
		}
	}

	s.mount.RemoveDir(uploadsDir)

	return err
}

func (s *cephFSStore) writeUploadAt(ctx context.Context, upload string, p []byte, off int64) (n int, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("(*cephFSStore).writeUploadAt: %w (off=%d upload=%q)", err, off, upload)
		}
	}()

	file, err := uploadFilePath(upload)
	if err != nil {
		return 0, err
	}

	return s.pwrite(ctx, file, p, off)
}

func (s *cephFSStore) pwrite(ctx context.Context, file string, p []byte, off int64) (n int, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("(*cephFSStore).pwrite: %w (off=%d file=%q)", err, off, file)
		}
	}()

	// BUG(ls): figure out how to properly implement cancelation in libcephfs2
	fd, err := s.mount.Open(file, os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		return 0, err
	}
	defer func() {
		if cerr := fd.Close(); err == nil {
			err = cerr
		}
	}()

	return fd.WriteAt(p, off)
}

func (s *cephFSStore) sha256sum(ctx context.Context, upload string) ([]byte, error) {
	file, err := uploadFilePath(upload)
	if err != nil {
		return nil, err
	}
	dgst := sha256.New()

	fd, err := s.mount.Open(file, os.O_RDONLY, 0)
	if err != nil {
		return nil, err
	}
	defer fd.Close()

	if _, err := io.Copy(dgst, fd); err != nil {
		return nil, err
	}

	return dgst.Sum(nil), nil
}

func recPath(r string) (string, error) {
	cleaned := path.Clean(r) // process dot/dotdot and strip trailing slashes
	parent, child := path.Split(cleaned)
	if parent != "recordings/" || len(child) < 2 {
		return "", fmt.Errorf("invalid recording identifier: %q", r)
	}
	bucket := child[:2]
	return path.Join(parent, bucket, child), nil
}

func uploadFilePath(name string) (string, error) {
	parent, child, err := uploadNameSplit(name)
	if err != nil {
		return "", err
	}
	dir, err := recPath(parent)
	if err != nil {
		return "", err
	}
	return path.Join(dir, child), nil
}
