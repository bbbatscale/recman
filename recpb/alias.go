// Package recpb provides import path compatibility with older client code. New
// code should use bbbatscale.de/recman/recordings/v1 directly, which this
// package forwards to.
package recpb

import (
	recordingsv1 "bbbatscale.de/recman/recordings/v1"
)

const (
	ContentHash_SHA256               = recordingsv1.ContentHash_SHA256
	ContentHash_UNSPECIFIED          = recordingsv1.ContentHash_UNSPECIFIED
	Recording_ARCHIVED               = recordingsv1.Recording_ARCHIVED
	Recording_ARCHIVING              = recordingsv1.Recording_ARCHIVING
	Recording_ARCHIVING_FAILED       = recordingsv1.Recording_ARCHIVING_FAILED
	Recording_BROKEN                 = recordingsv1.Recording_BROKEN
	Recording_IN_PROGRESS            = recordingsv1.Recording_IN_PROGRESS
	Recording_PROCESSED              = recordingsv1.Recording_PROCESSED
	Recording_PROCESSING             = recordingsv1.Recording_PROCESSING
	Recording_PROCESSING_FAILED      = recordingsv1.Recording_PROCESSING_FAILED
	Recording_PUBLISHED              = recordingsv1.Recording_PUBLISHED
	Recording_PUBLISHING             = recordingsv1.Recording_PUBLISHING
	Recording_PUBLISHING_FAILED      = recordingsv1.Recording_PUBLISHING_FAILED
	Recording_READY                  = recordingsv1.Recording_READY
	Recording_SANITY_CHECKED         = recordingsv1.Recording_SANITY_CHECKED
	Recording_SANITY_CHECKING        = recordingsv1.Recording_SANITY_CHECKING
	Recording_SANITY_CHECKING_FAILED = recordingsv1.Recording_SANITY_CHECKING_FAILED
	Recording_UNKNOWN                = recordingsv1.Recording_UNKNOWN
	Recording_UPLOADED               = recordingsv1.Recording_UPLOADED
	Recording_UPLOADING              = recordingsv1.Recording_UPLOADING
	Recording_UPLOADING_FAILED       = recordingsv1.Recording_UPLOADING_FAILED
)

var (
	NewRecordingsClient      = recordingsv1.NewRecordingsClient
	NewUploadsClient         = recordingsv1.NewUploadsClient
	RegisterRecordingsServer = recordingsv1.RegisterRecordingsServer
	RegisterUploadsServer    = recordingsv1.RegisterUploadsServer

	ContentHash_Alg_name                = recordingsv1.ContentHash_Alg_name
	ContentHash_Alg_value               = recordingsv1.ContentHash_Alg_value
	File_recordings_v1_recordings_proto = recordingsv1.File_recordings_v1_recordings_proto
	Recording_State_name                = recordingsv1.Recording_State_name
	Recording_State_value               = recordingsv1.Recording_State_value
	Recordings_ServiceDesc              = recordingsv1.Recordings_ServiceDesc
	Uploads_ServiceDesc                 = recordingsv1.Uploads_ServiceDesc
)

type (
	ContentHash                   = recordingsv1.ContentHash
	ContentHash_Alg               = recordingsv1.ContentHash_Alg
	CreateRecordingRequest        = recordingsv1.CreateRecordingRequest
	CreateUploadRequest           = recordingsv1.CreateUploadRequest
	DeleteRecordingRequest        = recordingsv1.DeleteRecordingRequest
	DeleteUploadRequest           = recordingsv1.DeleteUploadRequest
	GetRecordingRequest           = recordingsv1.GetRecordingRequest
	GetUploadRequest              = recordingsv1.GetUploadRequest
	ListRecordingsRequest         = recordingsv1.ListRecordingsRequest
	ListRecordingsResponse        = recordingsv1.ListRecordingsResponse
	ListUploadsRequest            = recordingsv1.ListUploadsRequest
	ListUploadsResponse           = recordingsv1.ListUploadsResponse
	Recording                     = recordingsv1.Recording
	Recording_State               = recordingsv1.Recording_State
	RecordingsClient              = recordingsv1.RecordingsClient
	RecordingsServer              = recordingsv1.RecordingsServer
	UnimplementedRecordingsServer = recordingsv1.UnimplementedRecordingsServer
	UnimplementedUploadsServer    = recordingsv1.UnimplementedUploadsServer
	UnsafeRecordingsServer        = recordingsv1.UnsafeRecordingsServer
	UnsafeUploadsServer           = recordingsv1.UnsafeUploadsServer
	UpdateRecordingRequest        = recordingsv1.UpdateRecordingRequest
	UpdateUploadRequest           = recordingsv1.UpdateUploadRequest
	Upload                        = recordingsv1.Upload
	UploadsClient                 = recordingsv1.UploadsClient
	UploadsServer                 = recordingsv1.UploadsServer
	Uploads_StreamingWriteClient  = recordingsv1.Uploads_StreamingWriteClient
	Uploads_StreamingWriteServer  = recordingsv1.Uploads_StreamingWriteServer
	WriteRequest                  = recordingsv1.WriteRequest
	WriteResponse                 = recordingsv1.WriteResponse
)
