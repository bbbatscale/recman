package main

import (
	"bytes"
	"context"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"strings"
	"sync"

	pb "bbbatscale.de/recman/recpb"
	"github.com/mennanov/fmutils"
	bolt "go.etcd.io/bbolt"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type uploadService struct {
	pb.UnimplementedUploadsServer

	db    *bolt.DB
	store *cephFSStore
	tasks *runner
	log   *zap.Logger

	writingMu sync.Mutex
	writing   map[string]bool
}

var (
	recordingsBucket = []byte("recordings")
	uploadsBucket    = []byte("uploads")
	tasksBucket      = []byte("tasks")
)

func (u *uploadService) GetUpload(ctx context.Context, in *pb.GetUploadRequest) (out *pb.Upload, err error) {
	key := []byte(in.Name)
	err = u.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(uploadsBucket)
		p := b.Get(key)
		if p == nil {
			return os.ErrNotExist
		}
		value := new(pb.Upload)
		err = proto.Unmarshal(p, value)
		if err == nil {
			out = value
		}
		return err
	})
	if errors.Is(err, os.ErrNotExist) {
		return nil, status.Errorf(codes.NotFound, "no such upload: %q", in.Name)
	}
	if err != nil {
		return nil, status.Errorf(codes.Internal, "access store: %v", err)
	}

	return out, nil
}

func (u *uploadService) CreateUpload(ctx context.Context, in *pb.CreateUploadRequest) (*pb.Upload, error) {
	uploadID := randomString(16)

	// BUG(ls): verify that parent is valid. We first need to define what
	// valid is supposed to mean exactly.
	name := uploadName(in.Parent, uploadID)
	recordingKey := []byte(in.Parent)
	key := []byte(name)

	ts := timestamppb.Now()
	upload := &pb.Upload{
		Name:       name,
		Offset:     0,
		Length:     in.Upload.GetLength(),
		CreateTime: ts,
		UpdateTime: ts,
	}
	value, err := proto.Marshal(upload)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "marshal: %v", err)
	}
	var prevState pb.Recording_State
	err = u.db.Update(func(tx *bolt.Tx) error {
		br := tx.Bucket(recordingsBucket)
		p := br.Get(recordingKey)
		if p == nil {
			return os.ErrNotExist
		}
		var r pb.Recording
		if err := proto.Unmarshal(p, &r); err != nil {
			return err
		}
		if r.State >= pb.Recording_UPLOADED {
			return io.ErrClosedPipe
		}
		prevState = r.State
		r.State = pb.Recording_UPLOADING
		p, err := proto.Marshal(&r)
		if err != nil {
			return err
		}
		if err := br.Put(recordingKey, p); err != nil {
			return err
		}

		bu := tx.Bucket(uploadsBucket)
		return bu.Put(key, value)
	})
	if errors.Is(err, os.ErrNotExist) {
		return nil, status.Errorf(codes.NotFound, "parent does not exist")
	}
	if errors.Is(err, io.ErrClosedPipe) {
		return nil, status.Errorf(codes.FailedPrecondition, "already uploaded")
	}
	if err != nil {
		return nil, status.Errorf(codes.Internal, "access store: %v", err)
	}
	if err := u.store.prepareTree(ctx, in.Parent); err != nil {
		return nil, status.Errorf(codes.Internal, "access store: %v", err)
	}
	if prevState != pb.Recording_PUBLISHED {
		log.Printf("created upload for %s, previous state was %s", in.Parent, prevState)
	}

	return upload, nil
}

func (u *uploadService) UpdateUpload(ctx context.Context, in *pb.UpdateUploadRequest) (*pb.Upload, error) {
	upload := in.Upload
	if upload == nil {
		upload = &pb.Upload{}
	}
	mask := in.UpdateMask

	key := []byte(upload.GetName())
	parent, err := uploadParent(upload.GetName())
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "retrieve parent: %v", err)
	}
	recordingKey := []byte(parent)
	if mask.Normalize(); !mask.IsValid(upload) {
		return nil, status.Errorf(codes.InvalidArgument, "field mask not valid for upload")
	}
	fmutils.Filter(upload, mask.Paths)
	fmutils.Prune(upload, []string{"create_time", "update_time"}) // read-only fields

	finalize := upload.Done
	var updated *pb.Upload
	var alreadyWritten int64
	err = u.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(uploadsBucket)
		p := b.Get(key)
		if p == nil {
			return os.ErrNotExist
		}
		stored := new(pb.Upload)
		if err := proto.Unmarshal(p, stored); err != nil {
			return err
		}
		if stored.Done {
			return io.ErrClosedPipe
		}
		if off := upload.GetOffset(); off != 0 && off != stored.Offset {
			return errImmutableField
		}
		if finalize {
			total := upload.GetLength()
			if total < stored.Offset {
				alreadyWritten = stored.Offset
				return os.ErrInvalid
			}
			br := tx.Bucket(recordingsBucket)
			recp := br.Get(recordingKey)
			if recp == nil {
				return errors.New("parent deleted")
			}
			var r pb.Recording
			if err := proto.Unmarshal(recp, &r); err != nil {
				return err
			}
			if r.State >= pb.Recording_UPLOADED {
				return io.ErrClosedPipe
			}
			r.State = pb.Recording_UPLOADED
			r.UpdateTime = timestamppb.Now()
			recp, err := proto.Marshal(&r)
			if err != nil {
				return err
			}
			if err := br.Put(recordingKey, recp); err != nil {
				return err
			}
		}
		proto.Merge(stored, upload)
		stored.UpdateTime = timestamppb.Now()

		newVal, err := proto.Marshal(stored)
		if err != nil {
			return err
		}
		if err := b.Put(key, newVal); err != nil {
			return err
		}
		updated = stored
		return nil
	})
	if errors.Is(err, os.ErrNotExist) {
		return nil, status.Errorf(codes.NotFound, "no such upload: %q", key)
	}
	if errors.Is(err, io.ErrClosedPipe) {
		return nil, status.Errorf(codes.FailedPrecondition, "upload already closed: %q", key)
	}
	if errors.Is(err, os.ErrInvalid) {
		return nil, status.Errorf(codes.OutOfRange, "attempt to set length %d, already written: %d",
			upload.Length, alreadyWritten)
	}
	if errors.Is(err, errImmutableField) {
		return nil, status.Errorf(codes.InvalidArgument, "upload %s: %v", key, err)
	}
	if err != nil {
		return nil, status.Errorf(codes.Internal, "access store: %v", err)
	}

	if finalize && u.tasks != nil {
		if err := u.tasks.signalTask(ctx, taskActionFinalize, parent); err != nil {
			return nil, status.Errorf(codes.Internal, "signal finalization: %v", err)
		}
	}

	return updated, nil
}

func (u *uploadService) DeleteUpload(ctx context.Context, in *pb.DeleteUploadRequest) (*emptypb.Empty, error) {
	key := []byte(in.Name)

	err := u.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(uploadsBucket)
		if p := b.Get(key); p == nil {
			return os.ErrNotExist
		}
		return b.Delete(key)
	})
	if errors.Is(err, os.ErrNotExist) {
		return nil, status.Errorf(codes.NotFound, "no such upload: %q", in.Name)
	}
	if err != nil {
		return nil, status.Errorf(codes.Internal, "access store: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (u *uploadService) ListUploads(ctx context.Context, in *pb.ListUploadsRequest) (*pb.ListUploadsResponse, error) {
	const firstElem = "recordings/"
	if p := in.Parent; p != "" {
		if !strings.HasPrefix(p, firstElem) || len(p) == len(firstElem) {
			return nil, status.Errorf(codes.InvalidArgument, "invalid parent spec: %q", p)
		}
	}
	var xs []*pb.Upload
	prefix := []byte(in.Parent + "/")
	err := u.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(uploadsBucket)
		c := b.Cursor()
		for k, v := c.Seek(prefix); bytes.HasPrefix(k, prefix); k, v = c.Next() {
			x := new(pb.Upload)
			if err := proto.Unmarshal(v, x); err != nil {
				return err
			}
			xs = append(xs, x)
		}
		return nil
	})
	if err != nil {
		return nil, status.Errorf(codes.Internal, "access store: %v", err)
	}

	out := &pb.ListUploadsResponse{
		Uploads: xs,
	}
	return out, nil
}

var errConcurrentWriter = errors.New("multiple concurrent write ops")

func (u *uploadService) Write(ctx context.Context, in *pb.WriteRequest) (*pb.WriteResponse, error) {
	upload, closeWrite, err := u.beginWrite(ctx, in)
	if err != nil {
		return nil, err
	}
	defer closeWrite()

	n, err := u.store.writeUploadAt(ctx, in.Name, in.Content, upload.Offset)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "write: %v", err)
	}

	off0 := upload.Offset
	upload.Offset = off0 + int64(n)
	upload.UpdateTime = timestamppb.Now()
	newValue, err := proto.Marshal(upload)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "marshal: %v", err)
	}

	key := []byte(in.Name)
	err = u.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(uploadsBucket)
		return b.Put(key, newValue)
	})
	if err != nil {
		return nil, status.Errorf(codes.Internal, "access store: %v", err)
	}

	out := &pb.WriteResponse{
		NewOffset: upload.Offset,
	}

	return out, nil
}

func (u *uploadService) StreamingWrite(stream pb.Uploads_StreamingWriteServer) error {
	in, err := stream.Recv()
	if err != nil {
		return err
	}

	upload, closeWrite, err := u.beginWrite(stream.Context(), in)
	if err != nil {
		return err
	}
	defer closeWrite()

	in0 := in
	woff := in0.Offset
	p := in0.Content
	for {
		for {
			in, err = stream.Recv()
			if err != nil {
				break
			}
			if in.Offset != woff+int64(len(p)) {
				return status.Errorf(codes.OutOfRange, "got offset %d, want %d",
					in.Offset, woff+int64(len(p)))
			}
			p = append(p, in.Content...)
			if len(p) >= 1<<20 {
				// write out to storage
				break
			}
		}
		var nw int
		var werr error
		if len(p) > 0 {
			nw, werr = u.store.writeUploadAt(stream.Context(), in0.Name, p, woff)
			if werr != nil {
				return status.Errorf(codes.Internal, "write: %v", err)
			}
		}
		woff += int64(nw)
		p = p[:0]
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
	}

	upload.Offset = woff
	upload.UpdateTime = timestamppb.Now()
	newValue, err := proto.Marshal(upload)
	if err != nil {
		return status.Errorf(codes.Internal, "marshal: %v", err)
	}

	key := []byte(in0.Name)
	err = u.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(uploadsBucket)
		return b.Put(key, newValue)
	})
	if err != nil {
		return status.Errorf(codes.Internal, "access store: %v", err)
	}

	out := &pb.WriteResponse{
		NewOffset: upload.Offset,
	}

	return stream.SendAndClose(out)
}

func (u *uploadService) beginWrite(ctx context.Context, in *pb.WriteRequest) (*pb.Upload, func(), error) {
	strKey := in.Name
	key := []byte(strKey)

	var upload *pb.Upload
	err := u.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(uploadsBucket)
		p := b.Get(key)
		if p == nil {
			return os.ErrNotExist
		}
		value := new(pb.Upload)
		err := proto.Unmarshal(p, value)
		if err == nil {
			if value.Done {
				return io.ErrClosedPipe
			}
			if value.Length != 0 && value.Offset+int64(len(in.Content)) > value.Length {
				return os.ErrInvalid
			}
			if value.Offset != in.Offset {
				return errConcurrentWriter
			}
			if !u.tryLock(strKey) {
				return errConcurrentWriter
			}
			upload = value
		}
		return err
	})
	if errors.Is(err, os.ErrNotExist) {
		return nil, nil, status.Errorf(codes.NotFound, "no such upload: %q", in.Name)
	}
	if errors.Is(err, io.ErrClosedPipe) {
		return nil, nil, status.Errorf(codes.FailedPrecondition, "upload already closed")
	}
	if errors.Is(err, os.ErrInvalid) {
		return nil, nil, status.Errorf(codes.OutOfRange, "write offset %d beyond total length", in.Offset)
	}
	if errors.Is(err, errConcurrentWriter) {
		return nil, nil, status.Error(codes.Aborted, "concurrent writer: retry with updated offset")
	}
	if err != nil {
		return nil, nil, status.Errorf(codes.Internal, "access store: %v", err)
	}

	closeFunc := func() {
		u.unlock(strKey)
	}
	return upload, closeFunc, nil
}

func uploadName(parent, uploadID string) string {
	return path.Join(parent, "uploads", uploadID)
}

func (u *uploadService) tryLock(key string) bool {
	defer u.writingMu.Unlock()
	u.writingMu.Lock()

	if u.writing[key] {
		return false
	}
	if u.writing == nil {
		u.writing = make(map[string]bool)
	}
	u.writing[string(key)] = true
	return true

}
func (u *uploadService) unlock(key string) {
	u.writingMu.Lock()
	delete(u.writing, key)
	u.writingMu.Unlock()
}

// RandomString generates a printable random string of length n using a
// cryptographically-secure RNG.
func randomString(n int) string {
	scratch := make([]byte, (n+3)/4*3)
	if _, err := rand.Read(scratch); err != nil {
		panic(err)
	}

	return base64.URLEncoding.EncodeToString(scratch)[:n]
}

func uploadParent(upload string) (string, error) {
	p, _, err := uploadNameSplit(upload)
	return p, err
}

func uploadNameSplit(upload string) (string, string, error) {
	parent, uploadID := path.Split(path.Clean(upload))
	if !strings.HasPrefix(parent, "recordings/") {
		return "", "", fmt.Errorf("invalid upload identifier: %q", upload)
	}
	parent, child := path.Split(path.Clean(parent))
	if child != "uploads" {
		return "", "", fmt.Errorf("invalid upload identifier: %q", upload)
	}

	return path.Clean(parent), path.Join("uploads", uploadID), nil
}
