syntax = "proto3";
// Package recordings.v1 describes services for tracking recordings
// amongst a fleet of BBB servers.
package recordings.v1;

import "google/protobuf/empty.proto";
import "google/protobuf/field_mask.proto";
import "google/protobuf/timestamp.proto";
import "google/api/annotations.proto";

// The Recordings service provides a resource-oriented interface for keeping
// track of recordings. The google.api.http annotations allow for automatic
// mapping to a more traditional HTTP interface (e.g. using grpc-gateway).
//
// A recording is identified by its recording ID and the parent collection it
// is a part of (always recordings/ for now). Hence, the full name for a
// recording with ID rec-id is "recordings/rec-id".
//
// BUG(ls): the creation of a recording associates it with an "origin",
// commonly the machine-id or hostname of the creating node. We probably want
// to attach some basic authz scheme to this to reduce the possibility of
// damage from bugs in the client code.
service Recordings {
	// GetRecording returns the recording referred to by name or returns an
	// error.
	//
	// Erorrs:
	// NOT_FOUND: no such recording
	rpc GetRecording(GetRecordingRequest) returns (Recording) {
		option (google.api.http) = { get: "/v1/recordings/{name}" };
	}
	// CreateRecording registers a new recording with the service.
	//
	// Errors:
	// ALREADY_EXISTS: a recording with name already exists
	// INVALID_ARGUMENT: recording is missing name,  origin or other
	// required field
	//
	// BUG(ls): allow caller to specify parent collection and think
	// whether we need more than a flat recordings namespace.
	rpc CreateRecording(CreateRecordingRequest) returns (Recording) {
		option (google.api.http) = {
			post: "/v1/recordings"
			body: "recording"
		};
	}
	// UpdateRecording updates those fields of the recording specified by
	// update_mask.
	//
	// Errors:
	// NOT_FOUND: no such recording
	// INVALID_ARGUMENT: update mask is invalid or specifies immutable field
	rpc UpdateRecording(UpdateRecordingRequest) returns (Recording) {
		option (google.api.http) = {
			patch: "/v1/recordings/*"
			body: "recording"
		};
	}

	// DeleteRecording drops the specified recording from the service.
	//
	// Errors:
	// NOT_FOUND: no such recording
	rpc DeleteRecording(DeleteRecordingRequest) returns (google.protobuf.Empty) {
		option (google.api.http) = { delete: "/v1/recordings/{name}" };
	}

	// ListRecordings returns a maximum of page_size recordings in the
	// collection referred to by parent. It returns a next_page_token that
	// can be passed to a subsequent call.
	rpc ListRecordings(ListRecordingsRequest) returns (ListRecordingsResponse) {
		option (google.api.http) = { get: "/v1/recordings" };
	}
}

message GetRecordingRequest {
	// Name identifies the requested resource (e.g. recordings/$id)
	string name = 1;
}

message CreateRecordingRequest {
	string parent = 1;
	Recording recording = 2;
}

message UpdateRecordingRequest {
	Recording recording = 1;
	google.protobuf.FieldMask update_mask = 2;
}

message DeleteRecordingRequest {
	// Name identifies the resource to delete
	string name = 1;
}

message ListRecordingsRequest {
	// Parent specifies the parent collection to list.
	// BUG(ls): maybe some kind of filter expression?
	string parent = 1;

	// Page_size specifies the maximum number of recordings to return.
	int32 page_size = 2;

	// Page_token describes the pagination offset, if any. Its value must
	// have been obtained from the response to a previous List request.
	string page_token = 3;
}

message ListRecordingsResponse {
	repeated Recording recordings = 1;

	// Next_page_token is an opaque string that may be used in a subsequent call
	// to request the next page.
	string next_page_token = 2;
}

message ContentHash {
	enum Alg {
		UNSPECIFIED = 0;
		SHA256 = 1;
	}
	Alg alg = 1;
	bytes value = 2;
}

message Recording {
	string name = 1;
	// opaque node identifier (commonly machine-id or hostname)
	string origin = 2;
	// BUG(ls): do we really want to expose the BBB recording states here?
	enum State {
		UNKNOWN = 0;
		IN_PROGRESS = 1;
		ARCHIVING = 2;
		ARCHIVING_FAILED = 3;
		ARCHIVED = 4;
		SANITY_CHECKING = 5;
		SANITY_CHECKING_FAILED = 6;
		SANITY_CHECKED = 7;
		PROCESSING = 8;
		PROCESSING_FAILED = 9;
		PROCESSED = 10;
		PUBLISHING = 11;
		PUBLISHING_FAILED = 12;
		PUBLISHED = 13;
		UPLOADING = 14;
		UPLOADING_FAILED = 15;
		UPLOADED = 16;
		READY = 17;
		BROKEN = 18;
	}
	State state = 3;
	string external_id = 4;
	string playback_url = 5;
	bytes metadata = 6;
	ContentHash content_hash = 7; // read-only

	google.protobuf.Timestamp create_time = 10; // read-only
	google.protobuf.Timestamp update_time = 11; // read-only
}

// The Uploads service allows opening uploads for a recording and tracking
// their progess.
//
// An upload is identified by its parent recording and an opaque upload
// identifier, e.g. "recordings/rec-id/uploads/upload-id".
service Uploads {
	// GetUpload returns the upload state identified by name.
	//
	// Errors:
	// NOT_FOUND: no such upload
	rpc GetUpload(GetUploadRequest) returns (Upload) {
		option (google.api.http) = { get: "/v1/{name=recordings/*/uploads/*}" };
	}

	// CreateUpload attaches an upload state to the recording specified by parent.
	rpc CreateUpload(CreateUploadRequest) returns (Upload) {
		option (google.api.http) = {
			post: "/v1/{parent=recordings/*}/uploads"
			body: "upload"
		};
	}

	// UpdateUpload updates those fields of the upload state specified by
	// update_mask.
	//
	// Writing true into done causes the upload to be finalized. If the
	// sha256 field is written simultaneously, it is compared against the
	// SHA-256 digest of the written upload data.
	//
	// Errors:
	// INVALID_ARGUMENT: update_mask is invalid or specifies immutable
	// field
	// NOT_FOUND: no such upload state
	// OUT_OF_RANGE: atttempt to set total length greater than current write
	// offset
	rpc UpdateUpload(UpdateUploadRequest) returns (Upload) {
		option (google.api.http) = {
			patch: "/v1/{name=recordings/*/uploads/*}"
			body: "upload"
		};
	}

	// DeleteUpload ...
	rpc DeleteUpload(DeleteUploadRequest) returns (google.protobuf.Empty) {
		option (google.api.http) = { delete: "/v1/{name=recordings/*/uploads/*}" };
	}

	// ListUploads ...
	rpc ListUploads(ListUploadsRequest) returns (ListUploadsResponse) {
		option (google.api.http) = { get: "/v1/{parent=recordings/*}/uploads" };
	}

	// Write implements an initial, super-simple, method for writing the
	// actual recording data.
	rpc Write(WriteRequest) returns (WriteResponse) {
		option (google.api.http) = {
			post: "/v1/{name=recordings/*/uploads/*}:write"
			body: "content"
		};
	}
	// Like Write but client-side streaming
	rpc StreamingWrite(stream WriteRequest) returns (WriteResponse) {}
}

message GetUploadRequest {
	// Name identifies the requested resource (e.g.
	// recordings/$rec_id/uploads/$upload_id)
	string name = 1;
}

message CreateUploadRequest {
	string parent = 1; // the recording for which an upload is requested
	Upload upload = 2;
}

message UpdateUploadRequest {
	Upload upload = 1;
	google.protobuf.FieldMask update_mask = 2;
}

message DeleteUploadRequest {
	// Name identifies the resource to delete
	string name = 1;
}

message ListUploadsRequest {
	string parent = 1;
}

message ListUploadsResponse {
	repeated Upload uploads = 1;
}

message Upload {
	string name = 1;
	int64 offset = 2; // current write offset in bytes
	int64 length = 3; // full upload size in bytes, or 0 if indeterminate
	// Whether the upload has completed. Writing true finalizes the upload,
	// turning it immutable and triggering the distribution pipeline.
	bool done = 4;
	bytes sha256 = 5;

	google.protobuf.Timestamp create_time = 10; // read-only
	google.protobuf.Timestamp update_time = 11; // read-only
}


message WriteRequest {
	string name = 1;
	int64 offset = 2;
	bytes content = 3;
}

message WriteResponse {
	int64 new_offset = 1;
}
