package main

import (
	"bytes"
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"net/url"
	"os"
	"path"
	"strings"

	pb "bbbatscale.de/recman/recpb"
	bolt "go.etcd.io/bbolt"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
)

type runner struct {
	db    *bolt.DB
	store *cephFSStore
	log   *zap.Logger

	taskq chan task

	// BUG(ls): Think of a better way to configure playback URLs.
	playbackBaseURL *url.URL
}

func newRunner(db *bolt.DB, store *cephFSStore) *runner {
	const taskqSize = 64

	return &runner{
		db:    db,
		store: store,
		log:   zap.NewNop(),
		taskq: make(chan task, taskqSize),
	}
}

//go:generate stringer -type=taskAction
type taskAction int32

const (
	taskActionUnknown taskAction = iota
	taskActionFinalize
)

type task struct {
	what      taskAction
	recording string
}

func (r *runner) taskSelect(ctx context.Context) (err error) {
	defer func() {
		r.log.Warn("return from taskSelect", zap.Error(err))
	}()

	for {
		select {
		case t := <-r.taskq:
			r.log.Info("starting task",
				zap.String("what", t.what.String()),
				zap.String("recording", t.recording),
			)
			go r.taskRun(ctx, t)
		case <-ctx.Done():
			return ctx.Err()
		}
	}
}

func (r *runner) taskRun(ctx context.Context, t task) (err error) {
	log := r.log.With(
		zap.String("what", t.what.String()),
		zap.String("recording", t.recording),
	)
	defer func() {
		if err != nil {
			err = fmt.Errorf("taskRun: %w (what=%s recording=%q)",
				err, t.what, t.recording)
		}
	}()

	switch t.what {
	case taskActionFinalize:
		log.Info("taskRun: finalize")
		if err := r.runFinalize(ctx, t); err != nil {
			log.Error("taskRun: finalize failed", zap.Error(err))
			return err
		}
		log.Info("taskRun: finalized")
	default:
		log.Error("taskRun: unknown action")
		return os.ErrInvalid

	}

	return r.clearTask(ctx, t)
}

func (r *runner) clearTask(ctx context.Context, t task) (err error) {
	defer func() {
		if err == nil {
			r.log.Info("task cleared",
				zap.String("what", t.what.String()),
				zap.String("recording", t.recording),
			)
			return
		}
		r.log.Error("clearing task failed",
			zap.Error(err),
			zap.String("what", t.what.String()),
			zap.String("recording", t.recording),
		)
		err = fmt.Errorf("clearTask: %w", err)

	}()
	err = r.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(tasksBucket)
		return b.Delete([]byte(t.recording))
	})
	return err
}

func (r *runner) signalTask(ctx context.Context, what taskAction, recording string) (err error) {
	defer func() {
		log := r.log.With(
			zap.Error(err),
			zap.String("what", what.String()),
			zap.String("recording", recording),
		)
		if err == nil {
			log.Info("signaled task")
		} else {
			log.Warn("signaling task failed")
			err = fmt.Errorf("signalTask: %w (what=%s recording=%q)",
				err, what, recording)
		}
	}()

	// Note the task in stable storage, then signal the queue
	err = r.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(tasksBucket)
		p := make([]byte, 4)
		binary.LittleEndian.PutUint32(p, uint32(what))
		return b.Put([]byte(recording), p)
	})
	if err != nil {
		return err
	}

	select {
	case r.taskq <- task{what: what, recording: recording}:
		return nil
	case <-ctx.Done():
		return ctx.Err()
	}
}

func (r *runner) runFinalize(ctx context.Context, t task) (err error) {
	log := r.log.With(
		zap.String("what", t.what.String()),
		zap.String("recording", t.recording),
	)
	defer func() {
		if err != nil {
			err = fmt.Errorf("runFinalize: %w (what=%s recording=%q)",
				err, t.what, t.recording)
		}
	}()

	var designated *pb.Upload
	prefix := []byte(t.recording + "/uploads/")
	err = r.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(uploadsBucket)
		c := b.Cursor()
		for k, v := c.Seek(prefix); bytes.HasPrefix(k, prefix); k, v = c.Next() {
			x := new(pb.Upload)
			if err := proto.Unmarshal(v, x); err != nil {
				return err
			}
			if x.Done {
				designated = x
				return nil
			}
		}
		return nil
	})
	if err != nil {
		return err
	}
	if designated == nil {
		return errors.New("cannot finalize: no completed upload")
	}
	log = log.With(zap.String("upload", designated.Name))
	log.Info("running finalize")

	var hash *pb.ContentHash
	if len(designated.Sha256) > 0 {
		log.Info("finalize: verifying provided hash")
		dgst, err := r.store.sha256sum(ctx, designated.Name)
		if err != nil {
			return err
		}
		if !bytes.Equal(dgst, designated.Sha256) {
			return fmt.Errorf("hash mismatch: got %x, want %x", dgst, designated.Sha256)
		}
		hash = &pb.ContentHash{
			Alg:   pb.ContentHash_SHA256,
			Value: dgst,
		}
		log.Info("finalize: hashes match")
	}

	log.Info("finalize: closing tree")
	err = r.store.finalizeTree(ctx, t.recording, designated.Name)
	if err != nil {
		return err
	}
	log.Info("finalize: closed tree, marking ready")
	recKey := []byte(t.recording)
	playbackURL := *r.playbackBaseURL
	playbackURL.Path = path.Join(
		playbackURL.Path,
		strings.TrimPrefix(t.recording, "recordings/"),
	)
	theURL := playbackURL.String()
	err = r.db.Update(func(tx *bolt.Tx) error {
		// we can drop all uploads
		b := tx.Bucket(uploadsBucket)
		c := b.Cursor()
		for k, _ := c.Seek(prefix); bytes.HasPrefix(k, prefix); k, _ = c.Next() {
			b.Delete(k)
		}
		x := new(pb.Recording)
		br := tx.Bucket(recordingsBucket)
		p := br.Get(recKey)
		if err := proto.Unmarshal(p, x); err != nil {
			return err
		}
		x.State = pb.Recording_READY
		x.PlaybackUrl = theURL
		if hash != nil {
			x.ContentHash = hash
		}
		newp, err := proto.Marshal(x)
		if err != nil {
			return err
		}
		return br.Put(recKey, newp)
	})
	if err != nil {
		log.Error("finalize: update db failed", zap.Error(err))
		return err
	}
	log.Info("finalize: marked ready")
	return nil
}

func (r *runner) taskInit(ctx context.Context) (err error) {
	var tasks []task
	err = r.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(tasksBucket)
		return b.ForEach(func(k, v []byte) error {
			if len(v) != 4 {
				return fmt.Errorf("corrupted task entry: %q -> %x", string(k), v)
			}
			t := task{
				what:      taskAction(binary.LittleEndian.Uint32(v)),
				recording: string(k),
			}
			tasks = append(tasks, t)
			return nil
		})
	})
	if err != nil {
		return fmt.Errorf("taskInit: %w", err)
	}
	r.log.Sugar().Infof("taskInit: recovering %d task entries", len(tasks))
	for _, t := range tasks {
		if err := r.signalTask(ctx, t.what, t.recording); err != nil {
			return err
		}
	}
	return nil
}
