package main

import (
	"bytes"
	"context"
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"os"
	"path"
	"time"

	bolt "go.etcd.io/bbolt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/grpclog"
	healthpb "google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/reflection"

	"bbbatscale.de/recman/recpb"
)

var (
	certFile = flag.String("cert", os.Getenv("RECMAN_CERT"),
		"PEM-encoded X.509 certificate for TLS")
	keyFile = flag.String("key", os.Getenv("RECMAN_KEY"),
		"private key corresponding to cert")
	trustFile = flag.String("trust", os.Getenv("RECMAN_TRUST"),
		"trust anchors to verify client certificates against")
	dbFile = flag.String("db", os.Getenv("RECMAN_DB"),
		"file system path to recordings DB")
	laddr = flag.String("listen", os.Getenv("RECMAN_LISTEN"),
		"local `addr` for serving gRPC")
	cephSub = flag.String("cephsub", os.Getenv("RECMAN_CEPH_SUB"),
		"Ceph file system path to use as root")
	playbackBaseURL = flag.String("baseurl", os.Getenv("RECMAN_PLAYBACK_BASE_URL"),
		"playback base URL for new recordings")
	insecure       = flag.Bool("insecure", false, "run without TLS")
	dumpDBInterval = flag.Duration("dumpdb",
		parseDuration(os.Getenv("RECMAN_DUMPDB_INTERVAL")),
		"dump recordings database in intervals of `duration`")
	debugEnabled = flag.Bool("debug", os.Getenv("RECMAN_DEBUG") != "",
		"enable RPC debugging and tracing")
)

func main() {
	flag.Parse()
	logger, err := zap.NewProduction()
	if err != nil {
		// Just tear it down already.
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
	zap.RedirectStdLog(logger)
	grpclog.SetLoggerV2(zapgrpc.NewLogger(logger))
	log := logger.Sugar()
	grpc.EnableTracing = *debugEnabled

	var serverOpts []grpc.ServerOption
	if *insecure {
		log.Warn("insecure!")
	} else {
		tlsConf, err := makeTLSConfig(*certFile, *keyFile, *trustFile)
		if err != nil {
			log.Fatal(err)
		}
		serverOpts = append(serverOpts, grpc.Creds(
			credentials.NewTLS(tlsConf),
		))
	}

	db, err := bolt.Open(*dbFile, 0644, nil)
	if err != nil {
		log.Fatal(err)
	}

	// XXX use boltKV in uploadService!
	for _, b := range [][]byte{recordingsBucket, uploadsBucket, tasksBucket} {
		if (&boltKV{db: db, bucket: b}).ensureBucketExists(); err != nil {
			panic(err)
		}
	}
	ceph, err := mountCephFS(context.Background(), *cephSub)
	if err != nil {
		log.Fatal(err)
	}
	defer ceph.unmount(context.Background())

	baseURL, err := url.Parse(*playbackBaseURL)
	if err != nil {
		log.Fatal(err)
	}
	taskRunner := newRunner(db, ceph)
	taskRunner.log = logger.Named("tasks")
	taskRunner.playbackBaseURL = baseURL
	go taskRunner.taskSelect(context.Background())

	if err := taskRunner.taskInit(context.Background()); err != nil {
		log.Fatal(err)
	}

	uploads := &uploadService{
		db:    db,
		store: ceph,
		tasks: taskRunner,
		log:   logger.Named("uploads"),
	}

	recMan := newRecMan(&boltKV{db: db, bucket: recordingsBucket})
	recMan.log = logger.Named("recordings")
	s := grpc.NewServer(serverOpts...)
	recpb.RegisterRecordingsServer(s, recMan)
	recpb.RegisterUploadsServer(s, uploads)
	healthpb.RegisterHealthServer(s, &healthChecker{recordings: recMan})
	reflection.Register(s)
	log.Info("listen on", *laddr)
	lis, err := net.Listen("tcp", *laddr)
	if err != nil {
		log.Fatal(err)
	}

	if *debugEnabled {
		// make tracing endpoints available that were registered on the
		// DefaultMux
		go http.ListenAndServe("localhost:8080", nil)
	}

	if d := *dumpDBInterval; d > 0 {
		go func() {
			dumper := &dbDumper{
				man:  recMan,
				ceph: ceph,
				path: "backups",
			}
			if err := ceph.mkdirp(context.Background(), dumper.path, 0755); err != nil {
				log.Fatal(err)
			}
			for range time.Tick(d) {
				updated, err := dumper.dumpDB()
				if err != nil {
					log.Error("dumpdb:", err)
					continue
				}
				if !updated {
					log.Info("dumpdb: no change, skipping")
				}
			}
		}()
	}

	log.Fatal(s.Serve(lis))
}

type dbDumper struct {
	man  *recMan
	ceph *cephFSStore
	prev [sha256.Size]byte
	path string
}

func (d *dbDumper) dumpDB() (bool, error) {
	var buf bytes.Buffer

	t := time.Now()
	if err := d.man.dumpRecordingsJSON(context.Background(), &buf); err != nil {
		return false, err
	}
	p := buf.Bytes()
	if len(p) == 0 {
		return false, nil
	}
	new := sha256.Sum256(p)
	if new == d.prev {
		return false, nil
	}
	d.prev = new

	fname := path.Join(
		d.path,
		fmt.Sprintf("recordings.db-%s.json", t.Format(time.RFC3339)),
	)
	_, err := d.ceph.pwrite(context.Background(), fname, p, 0)
	return true, err
}

func parseDuration(s string) time.Duration {
	if d, err := time.ParseDuration(s); err == nil {
		return d
	}
	return 0
}

func makeTLSConfig(certFile, keyFile, caFile string) (tlsConf *tls.Config, err error) {
	defer func() {
		if err != nil {
			err = fmt.Errorf("makeTLSConfig(%q, %q, %q): %w",
				certFile, keyFile, caFile, err)
		}
	}()
	cert, err := tls.LoadX509KeyPair(certFile, keyFile)
	if err != nil {
		return nil, err
	}
	pemBytes, err := os.ReadFile(caFile)
	if err != nil {
		return nil, err
	}
	caCerts := x509.NewCertPool()
	if !caCerts.AppendCertsFromPEM(pemBytes) {
		// Not a real error in the sense that we can't work without it.
		// Almost certainly not what the user intended though, as no
		// one will be able to authenticate against the service.
		return nil, fmt.Errorf("no trust anchors read from %q", caFile)
	}

	tlsConf = &tls.Config{
		Certificates: []tls.Certificate{cert},
		ClientCAs:    caCerts,
		ClientAuth:   tls.RequireAndVerifyClientCert,
		MinVersion:   tls.VersionTLS12,
	}

	return tlsConf, nil
}
