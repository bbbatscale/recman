package main

import (
	"context"
	"errors"
	"io"
	"os"

	pb "bbbatscale.de/recman/recpb"
	"github.com/mennanov/fmutils"
	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/encoding/prototext"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type recMan struct {
	pb.UnimplementedRecordingsServer
	db  kv
	log *zap.Logger
}

func newRecMan(db kv) *recMan {
	return &recMan{db: db, log: zap.NewNop()}
}

func (m *recMan) GetRecording(ctx context.Context, in *pb.GetRecordingRequest) (out *pb.Recording, err error) {
	key := []byte(in.Name)
	value, err := m.db.get(ctx, key)
	if errors.Is(err, os.ErrNotExist) {
		return nil, status.Errorf(codes.NotFound, "no such recording: %q", in.Name)
	}
	if err != nil {
		return nil, status.Errorf(codes.Internal, "access store: %v", err)
	}

	out = new(pb.Recording)
	if err := proto.Unmarshal(value, out); err != nil {
		return nil, status.Errorf(codes.Internal, "unmarshal: %v", err)
	}
	return out, nil

}
func (m *recMan) CreateRecording(ctx context.Context, in *pb.CreateRecordingRequest) (*pb.Recording, error) {
	r := in.Recording
	if r.GetName() == "" || r.GetOrigin() == "" {
		return nil, status.Errorf(codes.InvalidArgument, "recording is missing name and/or origin")
	}

	key := []byte(r.Name)
	fmutils.Prune(r, []string{"content_hash", "create_time", "update_time"})
	ts := timestamppb.Now()
	r.CreateTime = ts
	r.UpdateTime = ts
	value, err := proto.Marshal(r)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "marshal: %v", err)
	}

	err = m.db.create(ctx, key, value)
	if errors.Is(err, os.ErrExist) {
		return nil, status.Errorf(codes.AlreadyExists, "recording already exists: %q", r.GetName())
	}
	if err != nil {
		return nil, status.Errorf(codes.Internal, "access store: %v", err)
	}

	return r, nil
}

var errImmutableField = errors.New("attempt to change immutable field")

func (m *recMan) UpdateRecording(ctx context.Context, in *pb.UpdateRecordingRequest) (*pb.Recording, error) {
	r := in.Recording
	mask := in.UpdateMask
	if r.GetName() == "" || r.GetOrigin() == "" || mask == nil {
		return nil, status.Errorf(codes.InvalidArgument, "recording is missing name and/or origin")
	}
	key := []byte(r.Name)
	if mask.Normalize(); !mask.IsValid(r) {
		return nil, status.Errorf(codes.InvalidArgument, "field mask not valid for recording")
	}
	fmutils.Filter(r, mask.Paths)
	fmutils.Prune(r, []string{"content_hash", "create_time", "update_time"})

	var updated *pb.Recording
	var triedToMoveBackwards bool
	var saveOld, saveNew pb.Recording_State
	err := m.db.update(ctx, key, func(old []byte) ([]byte, error) {
		stored := new(pb.Recording)
		if err := proto.Unmarshal(old, stored); err != nil {
			return nil, err
		}
		if orig := r.GetOrigin(); orig != "" && orig != stored.Origin {
			return nil, errImmutableField
		}
		// Ensure recording state doesn't move backwards. We used to
		// accept it so we can't just start failing those requests.
		// Ignoring the field update should be safe though.
		if stored.State > r.State {
			saveOld = stored.State
			saveNew = r.State
			triedToMoveBackwards = true
			r.State = stored.State
		}
		proto.Merge(stored, r)
		stored.UpdateTime = timestamppb.Now()
		updated = stored
		return proto.Marshal(stored)
	})
	if triedToMoveBackwards {
		m.log.Warn("request tried to move state backwards",
			zap.String("old", saveOld.String()),
			zap.String("new", saveNew.String()))
	}
	if errors.Is(err, os.ErrNotExist) {
		return nil, status.Errorf(codes.NotFound, "no such recording: %q", r.Name)
	}
	if errors.Is(err, errImmutableField) {
		return nil, status.Errorf(codes.InvalidArgument, "recording %s: %v", r.Name, err)
	}
	if err != nil {
		return nil, status.Errorf(codes.Internal, "access store: %v", err)
	}

	return updated, nil
}

func (m *recMan) DeleteRecording(ctx context.Context, in *pb.DeleteRecordingRequest) (*emptypb.Empty, error) {
	key := []byte(in.Name)
	err := m.db.delete(ctx, key)
	if errors.Is(err, os.ErrNotExist) {
		return nil, status.Errorf(codes.NotFound, "no such recording: %q", in.Name)
	}
	if err != nil {
		return nil, status.Errorf(codes.Internal, "access store: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (m *recMan) ListRecordings(ctx context.Context, in *pb.ListRecordingsRequest) (*pb.ListRecordingsResponse, error) {
	var xs []*pb.Recording

	err := m.db.iter(ctx, func(k, v []byte) error {
		x := new(pb.Recording)
		if err := proto.Unmarshal(v, x); err != nil {
			return err
		}
		xs = append(xs, x)
		return nil
	})
	if err != nil {
		return nil, status.Errorf(codes.Internal, "access store: %v", err)

	}

	out := &pb.ListRecordingsResponse{
		Recordings: xs,
	}

	return out, nil
}

func (m *recMan) dumpRecordingsJSON(ctx context.Context, w io.Writer) error {
	x := new(pb.Recording)
	return m.db.iter(ctx, func(k, v []byte) error {
		proto.Reset(x)
		if err := proto.Unmarshal(v, x); err != nil {
			return err
		}
		p, err := protojson.Marshal(x)
		if err != nil {
			return err
		}
		_, err = w.Write(append(p, '\n'))
		return err
	})
}

func (m *recMan) dumpRecordingsText(ctx context.Context, w io.Writer) error {
	x := new(pb.Recording)
	return m.db.iter(ctx, func(k, v []byte) error {
		proto.Reset(x)
		if err := proto.Unmarshal(v, x); err != nil {
			return err
		}
		p, err := prototext.Marshal(x)
		if err != nil {
			return err
		}
		_, err = w.Write(append(p, '\n'))
		return err
	})
}
