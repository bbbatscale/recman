package main

import (
	"context"
	"os"
	"sync"

	bolt "go.etcd.io/bbolt"
)

type kv interface {
	// Create stores value at key. If key already exists, the returned
	// error satisfies errors.Is(err, os.ErrExist).
	create(ctx context.Context, key, value []byte) error

	// Get retrieves the value stored for key or returns an error. If key
	// does not exist, the error satisfies errors.Is(err, os.ErrNotExist).
	get(ctx context.Context, key []byte) ([]byte, error)

	// Update invokes the provided updater with the existing value stored
	// for key. If updater returns with a non-nil error, the stored value
	// is updated with the returned byte slice (allowing for transactional
	// read-modify-write operations).
	// If key does not exist, updater is not invoked and update return an
	// error satisfying errors.Is(err, os.ErrNotExist).
	update(ctx context.Context, key []byte, updater func(old []byte) ([]byte, error)) error

	// Delete drops the value stored for key, if any.  If key does not
	// exist, the returned error satisfies errors.Is(err, os.ErrNotExist).
	delete(ctx context.Context, key []byte) error

	// Iter invokes fn with each stored key/value pair. Iterations stops
	// when fn returns a non-nil error.
	// BUG(ls): iter does not provide a way for range/subset queries.
	iter(ctx context.Context, fn func(k, v []byte) error) error
}

// BoltKV provides a simple kv implementation for single-replica deployments.
type boltKV struct {
	db     *bolt.DB
	bucket []byte

	createBucketOnce sync.Once
	createBucketErr  error
}

func (b *boltKV) ensureBucketExists() error {
	b.createBucketOnce.Do(func() {
		b.createBucketErr = b.db.Update(func(tx *bolt.Tx) error {
			_, err := tx.CreateBucketIfNotExists(b.bucket)
			return err
		})
	})
	return b.createBucketErr
}

func (b *boltKV) create(ctx context.Context, key, value []byte) error {
	if err := b.ensureBucketExists(); err != nil {
		return err
	}
	return b.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(b.bucket)
		p := b.Get(key)
		if p != nil {
			return os.ErrExist
		}
		return b.Put(key, value)
	})
}

func (b *boltKV) get(ctx context.Context, key []byte) ([]byte, error) {
	if err := b.ensureBucketExists(); err != nil {
		return nil, err
	}

	var value []byte
	err := b.db.View(func(tx *bolt.Tx) error {
		p := tx.Bucket(b.bucket).Get(key)
		if p == nil {
			return os.ErrNotExist
		}
		value = make([]byte, len(p))
		copy(value, p)
		return nil
	})
	return value, err
}

func (b *boltKV) update(ctx context.Context, key []byte, updater func(old []byte) ([]byte, error)) error {
	if err := b.ensureBucketExists(); err != nil {
		return err
	}
	return b.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(b.bucket)
		old := b.Get(key)
		if old == nil {
			return os.ErrNotExist
		}
		new, err := updater(old)
		if err != nil {
			return err
		}
		return b.Put(key, new)
	})
}

func (b *boltKV) delete(ctx context.Context, key []byte) error {
	if err := b.ensureBucketExists(); err != nil {
		return err
	}
	return b.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(b.bucket)
		p := b.Get(key)
		if p == nil {
			return os.ErrNotExist
		}
		return b.Delete(key)
	})

}

func (b *boltKV) iter(ctx context.Context, fn func(k, v []byte) error) error {
	if err := b.ensureBucketExists(); err != nil {
		return err
	}
	return b.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(b.bucket)
		return b.ForEach(fn)
	})
}

// EtcdKV implements kv using the etcd distributed key/value store. It is the
// preferred implementation for multi-replica deployments.
// TODO
type etcdKV struct{}
