# The recman Recordings Manager

Recman provides a gRPC service that its clients may use for tracking the state
of BBB recordings, eventually uploading them to stable storage.

For now, the only storage backend is CephFS via libcephfs2 and [its Go
bindings](https://pkg.go.dev/github.com/ceph/go-ceph@v0.13.0/cephfs).
MRs implementing additional storage options welcome.

## recserve

The recserve companion program delivers uploaded recording data to viewers over
HTTP. It may be built with an included copy of [BBB's standalone recording player](https://github.com/bigbluebutton/bbb-playback).
