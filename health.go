package main

import (
	"context"

	"bbbatscale.de/recman/recpb"
	"google.golang.org/grpc/codes"
	healthpb "google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/status"
)

// HealthChecker implements the grpc.health.v1 Health service for recMan and
// uploadsService.
type healthChecker struct {
	healthpb.UnimplementedHealthServer

	recordings *recMan
}

func (c *healthChecker) Check(ctx context.Context, in *healthpb.HealthCheckRequest) (*healthpb.HealthCheckResponse, error) {
	result := new(healthpb.HealthCheckResponse)

	switch in.Service {
	case "":
		fallthrough
	case "recordings.v1.Recordings":
		req := &recpb.ListRecordingsRequest{
			PageSize: 1,
		}
		result.Status = healthpb.HealthCheckResponse_NOT_SERVING
		if _, err := c.recordings.ListRecordings(ctx, req); err == nil {
			result.Status = healthpb.HealthCheckResponse_SERVING
		}
	case "recordings.v1.Uploads":
		return nil, status.Error(codes.Unimplemented, "not implemented")
	default:
		return nil, status.Error(codes.NotFound, "service not found")
	}

	return result, nil
}
